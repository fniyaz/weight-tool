import argparse
import pandas as pd
import numpy as np
import sklearn.linear_model
from datetime import datetime


APPLE_HEALTH_BODY_MASS_RECORD_TYPE = 'HKQuantityTypeIdentifierBodyMass'


def supported_datetime_formats() -> str:
    return f'a timestamp or a datetime in YYYY-MM-DD format'


def to_datetime(input: str) -> float:
    try:
        return float(input)
    except ValueError:
        try:
            return datetime.fromisoformat(input).timestamp()
        except ValueError:
            raise ValueError(f'Excepted {supported_datetime_formats()} got "{input}"')


# For argparse to display error nicely
to_datetime.__name__ = "datetime"


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Predict you weight from data exported from Apple Health using a Linear Regression.'
    )
    parser.add_argument('file')
    parser.add_argument('-t', '--filetype', choices=['apple_health'], default='apple_health')
    parser.add_argument('-s', '--start', type=to_datetime, help=f'a start date for filtering input as {supported_datetime_formats()}')
    parser.add_argument('-e', '--end', type=to_datetime, help=f'an end date for filtering input as {supported_datetime_formats()}')
    parser.add_argument('-d', '--dates', nargs='+', metavar='date', type=to_datetime, help=supported_datetime_formats())
    parser.add_argument('-r', '--range', nargs=2, metavar=('start', 'end'), type=to_datetime, help='daily range')
    args = parser.parse_args()

    return args


def parse_apple_health(file: str) -> np.ndarray:
    df = pd.read_xml(file, xpath=f'/HealthData/Record');
    df = df[df.type == APPLE_HEALTH_BODY_MASS_RECORD_TYPE].reindex(['creationDate', 'value'], axis=1)
    df.creationDate = df.creationDate.apply(lambda d: int(datetime.strptime(d, "%Y-%m-%d %H:%M:%S %z").timestamp()))
    return df.to_numpy(dtype=np.float32)


def parse_file(args: argparse.Namespace) -> np.ndarray:
    match args.filetype:
        case "apple_health":
            return parse_apple_health(args.file)
        case _:
            raise ValueError(f"File type {args.filetype} not supported.")


def filter_data(data: np.ndarray, args: argparse.Namespace) -> np.ndarray:
    if args.start:
        data = data[data[:, 0] >= args.start]
    if args.end:
        data = data[data[:, 0] <= args.end]

    return data


def prepare_prediction_input(args: argparse.Namespace) -> np.ndarray:
    dates = []
    if args.range:
        date = args.range[0]
        while date < args.range[1]:
            dates.append(date)
            date += 24*60*60
    if args.dates:
        dates += args.dates
    return np.array(dates).reshape(-1, 1)


def make_regression(X: np.ndarray, Y: np.ndarray) -> sklearn.linear_model.LinearRegression:
    reg = sklearn.linear_model.LinearRegression()
    reg.fit(X, Y)

    return reg


def main():
    args = parse_args()

    data = parse_file(args)
    data = filter_data(data, args)
    model = make_regression(data[:, 0].reshape(-1, 1), data[:, 1])
    X = prepare_prediction_input(args)
    if X.size > 0:
        Y = model.predict(X)

        for d, w in zip(X[:, 0], Y):
            print(datetime.fromtimestamp(d).strftime('%Y-%m-%d %H:%M:%S %z'), w)
        print()

    print(f"You are chaging weight at a rate of: {model.coef_[0]*60*60*24*7} kg/week")


if __name__ == '__main__':
    main()

